"""
Dataset reader
"""
import pandas as pd


def read_memes(filepath):
    df = pd.read_json(filepath)
    df = df[df.img_alt != '']
    return df

"""
Functions related to the classifier
"""
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn import metrics as sk_mtr


def train_model(model, dataset, epochs, callbacks=None):
    model.compile(
        optimizer='adam',
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy']
    )

    history = model.fit(
        x=dataset.X_train,
        y=dataset.Y_train,
        epochs=epochs,
        validation_data=(dataset.X_test, dataset.Y_test),
        callbacks=callbacks
    )

    return history


def evaluate_model(model, dataset):
    y_pred = model.predict(dataset.X_test)

    if len(y_pred.shape) > 1:  # For neural network based models with softmax
        y_pred = np.argmax(y_pred, axis=1)

    acc = sk_mtr.accuracy_score(dataset.Y_test, y_pred)
    precision = sk_mtr.precision_score(dataset.Y_test, y_pred, average='macro')
    recall = sk_mtr.recall_score(dataset.Y_test, y_pred, average='macro')
    f1 = sk_mtr.f1_score(dataset.Y_test, y_pred, average='macro')
    cm = sk_mtr.confusion_matrix(dataset.Y_test, y_pred)

    print('Accuracy:', np.round(acc * 100, 2), '%')
    print('Precision:', np.round(precision * 100, 2), '%')
    print('Recall:', np.round(recall * 100, 2), '%')
    print('F1 score:', np.round(f1 * 100, 2), '%')

    fig = plt.figure(figsize=(7, 7))
    ax = fig.subplots(nrows=1, ncols=1)
    sns.heatmap(cm, cmap='Greys', ax=ax)


def plot_learning_curves(train_history):
    fig = plt.figure(figsize=(15, 5))
    axs = fig.subplots(nrows=1, ncols=2)

    data = train_history.history
    common_kwargs = {
        'marker': 'o',
        'linestyle': '--'
    }

    axs[0].plot(data['loss'], label='Train', **common_kwargs)
    axs[0].plot(data['val_loss'], label='Val', **common_kwargs)
    axs[0].legend()
    axs[0].set_title('Loss')
    axs[0].set_xlabel('Epoch')
    axs[0].set_ylabel('Loss')
    axs[0].set_ylim((0, 1.1 * max(max(data['loss']), max(data['val_loss']))))

    axs[1].plot(data['acc'], label='Train', **common_kwargs)
    axs[1].plot(data['val_acc'], label='Val', **common_kwargs)
    axs[1].legend()
    axs[1].set_title('Accuracy')
    axs[1].set_xlabel('Epoch')
    axs[1].set_ylabel('Accuracy')
    axs[1].set_ylim((0, 1))


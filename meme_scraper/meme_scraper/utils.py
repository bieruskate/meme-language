from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst


class TakeFirstLoader(ItemLoader):
    default_output_processor = TakeFirst()


def get_value(entry, css_selector, default_value=None):
    val = entry.css(css_selector).extract_first()
    if not val:
        return default_value
    return val.encode('utf-8').strip()


def get_values(entry, css_selector):
    return [e.encode('utf-8').strip()
            for e in entry.css(css_selector).extract()]
